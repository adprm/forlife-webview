// import library
import NetInfo from "@react-native-community/netinfo";
import React, { useEffect, useState } from 'react';
import { Alert, PermissionsAndroid, StyleSheet, Text, View } from 'react-native';
import OneSignal from 'react-native-onesignal';
import { WebView } from 'react-native-webview';

// main function
const App = () => {
  const [connect, setConnet] = useState();

  // permission
  useEffect(() => {
    async function permission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: "Cool Photo App Camera Permission",
            message:
              "Cool Photo App needs access to your camera " +
              "so you can take awesome pictures.",
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the camera");
        } else {
          console.log("Camera permission denied");
        }
      } catch (error) {
        console.warn(error);
      }

      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: "IndoForlife",
            message:
              "IndoForlife access to your location",
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the location");
        } else {
          console.log("Location permission denied");
        }
      } catch (error) {
        console.warn(error);
      }
    }

    NetInfo.addEventListener(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
      setConnet(state.isConnected)
      if (state.isConnected !== true) {
        Alert.alert('Peringatan', 'Perangkat anda tidak terhubung ke internet.', [{
          text: 'Ya'
        }])
      }

    });

    permission()
  }, [])


  // main return
  return (
    <>
      {connect != true ? (
        <View style={styles.container}>
          <Text style={styles.font}>Tidak ada Internet.</Text>
        </View>
      ) : (
        <WebView
          source={{ uri: 'https://apps.indoforlife.com/' }}
          nativeConfig={{
            props: { webContentsDebuggingEnabled: true }
          }}
          domStorageEnabled={true}
          onMessage={(event) => {
            console.log('hereee', event);
            if (event.nativeEvent.data != 'exit') {
              OneSignal.setAppId("943a48ec-52ce-48c9-96d5-02e35f4b4484");
              OneSignal.setLogLevel(6, 0);
              OneSignal.setExternalUserId(event.nativeEvent.data, (results) => {
                // The results will contain push and email success statuses
                console.log('Results of setting external user id ' + event.nativeEvent.data);
                console.log(results);
              })
            } else if (event.nativeEvent.data == 'exit') {
              OneSignal.removeExternalUserId((results) => {
                // The results will contain push and email success statuses
                console.log('Results of removing external user id');
                console.log(results);
              })
            }
          }}
        />
      )}
    </>
  )
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
  font: {
    color: 'black'
  }

});
